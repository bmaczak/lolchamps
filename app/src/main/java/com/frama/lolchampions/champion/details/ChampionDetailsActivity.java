package com.frama.lolchampions.champion.details;

import javax.inject.Inject;

import com.frama.lolchampions.R;
import com.frama.lolchampions.champion.details.viewmodel.ChampionDetailsViewModel;
import com.frama.lolchampions.champion.list.model.ChampionDto;
import com.frama.lolchampions.databinding.ActivityChampionDetailsBinding;
import com.frama.lolchampions.di.components.DetailsActivityComponent;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Balazs_Maczak on 1/4/2017.
 */
public class ChampionDetailsActivity extends AppCompatActivity {

    private static String INTENT_EXTRA_CHAMPION_DTO = "champion_dto";

    @Inject
    ChampionDetailsViewModel viewModel;

    public static Intent createNavigationIntent(Activity activity, ChampionDto championDto) {
        Intent intent = new Intent(activity, ChampionDetailsActivity.class);
        intent.putExtra(INTENT_EXTRA_CHAMPION_DTO, championDto);
        return intent;
    }

    public static ChampionDto extractChampionDto(Intent intent) {
        return (ChampionDto) intent.getSerializableExtra(INTENT_EXTRA_CHAMPION_DTO);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DetailsActivityComponent.Injector.inject(this);
        ActivityChampionDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_champion_details);
        setSupportActionBar(binding.toolbar);
        binding.setViewModel(viewModel);
        viewModel.loadChampionDetails();
    }
}
