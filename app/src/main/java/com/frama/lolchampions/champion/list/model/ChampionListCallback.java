package com.frama.lolchampions.champion.list.model;

import java.util.List;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public interface ChampionListCallback {
    void onChampionsDownloaded(List<ChampionDto> champions);

    void onChampionsDownloadError(Throwable throwable);
}
