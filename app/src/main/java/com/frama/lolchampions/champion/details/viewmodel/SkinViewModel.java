package com.frama.lolchampions.champion.details.viewmodel;

import com.frama.lolchampions.champion.list.model.SkinDto;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class SkinViewModel extends ItemViewModel {
    private static String IMAGE_BASE_URL = "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/";

    private SkinDto skinDto;
    private String championId;

    public SkinViewModel(SkinDto skinDto, String championId) {
        this.skinDto = skinDto;
        this.championId = championId;
    }

    public String getName() {
        return skinDto.getName();
    }

    public String getImage() {
        return IMAGE_BASE_URL + championId + "_" + skinDto.getNum() + ".jpg";
    }
}
