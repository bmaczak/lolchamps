package com.frama.lolchampions.champion.list.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Balazs_Maczak on 2/8/2017.
 */
public class ChampionDto implements Serializable {
    private String id;
    private String title;
    private String name;
    private ImageDto image;
    private List<String> tags;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ImageDto getImage() {
        return image;
    }

    public void setImage(ImageDto image) {
        this.image = image;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
