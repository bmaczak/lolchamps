package com.frama.lolchampions.champion.list;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.frama.lolchampions.R;
import com.frama.lolchampions.champion.list.viewmodel.ChampionListViewModel;
import com.frama.lolchampions.databinding.ActivityListBinding;
import com.frama.lolchampions.di.components.ListActivityComponent;

import javax.inject.Inject;

/**
 * Created by Balazs_Maczak on 1/4/2017.
 */
public class ChampionListActivity extends AppCompatActivity {

    @Inject
    ChampionListViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListActivityComponent.Injector.inject(this);
        ActivityListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_list);
        setSupportActionBar(binding.toolbar);
        binding.setViewModel(viewModel);
        viewModel.loadChampions();
    }
}
