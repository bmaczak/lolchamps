package com.frama.lolchampions.champion.details.viewmodel;

import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class PageViewModel extends ItemViewModel {
    private ChampionDetailsModel model;

    public PageViewModel(ChampionDetailsModel model) {
        this.model = model;
    }

    public ChampionDetailsModel getModel() {
        return model;
    }
}
