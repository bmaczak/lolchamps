package com.frama.lolchampions.champion.details.viewmodel.pages;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.viewmodel.PageViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class LorePageViewModel extends PageViewModel {

    public LorePageViewModel(ChampionDetailsModel model) {
        super(model);
    }

    public Spanned getLore() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(getModel().getLore(), Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(getModel().getLore());
        }
    }

}
