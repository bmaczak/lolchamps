package com.frama.lolchampions.champion.details.model.dto;

import com.frama.lolchampions.champion.list.model.ImageDto;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class SpellDto {
    private String name;
    private String description;
    private ImageDto image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImageDto getImage() {
        return image;
    }

    public void setImage(ImageDto image) {
        this.image = image;
    }
}
