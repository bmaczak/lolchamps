package com.frama.lolchampions.champion.list.viewmodel;

import android.text.TextUtils;
import android.view.View;

import com.frama.lolchampions.champion.list.model.ChampionDto;
import com.frama.lolchampions.router.Router;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

/**
 * Created by Balazs_Maczak on 2/7/2017.
 */
public class ChampionListItemViewModel extends ItemViewModel {

    private static String IMAGE_BASE_URL = "http://ddragon.leagueoflegends.com/cdn/7.4.3/img/champion/";
    private static String TAG_DELIMITER = ",";

    private ChampionDto championDto;
    private Router router;

    public ChampionListItemViewModel(ChampionDto championDto, Router router) {
        this.championDto = championDto;
        this.router = router;
    }

    public String getName() {
        return championDto.getName();
    }

    public String getTitle() {
        return championDto.getTitle();
    }

    public String getImage() {
        return IMAGE_BASE_URL + championDto.getImage().getFull();
    }

    public String getTags() {
        return TextUtils.join(TAG_DELIMITER, championDto.getTags());
    }

    public void onClick(View v) {
        router.openChampionDetails(championDto);
    }

}
