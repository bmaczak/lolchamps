package com.frama.lolchampions.champion.details.model;

import com.frama.lolchampions.champion.details.model.dto.ChampionDetailsDto;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public interface ChampionDetailsCallback {
    void onChampionDetailsDownloaded(ChampionDetailsDto championDetailsDto);

    void onChampionDetailsDownloadError(Throwable throwable);
}
