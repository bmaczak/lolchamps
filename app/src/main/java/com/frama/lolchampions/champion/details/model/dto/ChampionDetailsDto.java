package com.frama.lolchampions.champion.details.model.dto;

import com.frama.lolchampions.champion.list.model.ImageDto;
import com.frama.lolchampions.champion.list.model.SkinDto;

import org.stringtemplate.v4.compiler.CompiledST;

import java.util.List;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class ChampionDetailsDto {
    private String id;
    private String key;
    private String name;
    private String title;
    private ImageDto image;
    private List<SkinDto> skins;
    private String lore;
    private String blurb;
    private List<String> allytips;
    private List<String> enemytips;
    private ChampionInfoDto info;
    private List<SpellDto> spells;
    private SpellDto passive;
    private ChampionStatsDto stats;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ImageDto getImage() {
        return image;
    }

    public void setImage(ImageDto image) {
        this.image = image;
    }

    public List<SkinDto> getSkins() {
        return skins;
    }

    public void setSkins(List<SkinDto> skins) {
        this.skins = skins;
    }

    public String getLore() {
        return lore;
    }

    public void setLore(String lore) {
        this.lore = lore;
    }

    public List<String> getAllytips() {
        return allytips;
    }

    public void setAllytips(List<String> allytips) {
        this.allytips = allytips;
    }

    public List<String> getEnemytips() {
        return enemytips;
    }

    public void setEnemytips(List<String> enemytips) {
        this.enemytips = enemytips;
    }

    public String getBlurb() {
        return blurb;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public ChampionInfoDto getInfo() {
        return info;
    }

    public void setInfo(ChampionInfoDto info) {
        this.info = info;
    }

    public List<SpellDto> getSpells() {
        return spells;
    }

    public void setSpells(List<SpellDto> spells) {
        this.spells = spells;
    }

    public SpellDto getPassive() {
        return passive;
    }

    public ChampionStatsDto getStats() {
        return stats;
    }
}

