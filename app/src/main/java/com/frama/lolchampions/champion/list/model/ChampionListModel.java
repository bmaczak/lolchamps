package com.frama.lolchampions.champion.list.model;

import java.util.ArrayList;
import java.util.Map;

import com.frama.lolchampions.api.ApiResponse;
import com.frama.lolchampions.api.RiotApi;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Balazs_Maczak on 2/21/2017.
 */
public class ChampionListModel {

    private RiotApi api;
    private Subscription subscription;
    private ChampionListCallback championListCallback;

    public ChampionListModel(RiotApi api) {
        this.api = api;
    }

    public void loadChampions() {
        if (subscription == null || subscription.isUnsubscribed()) {
            subscription = api.loadChampions()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNext, this::onError);
        }
    }

    private void onNext(ApiResponse<Map<String, ChampionDto>> response) {
        if (championListCallback != null) {
            championListCallback.onChampionsDownloaded(new ArrayList<>(response.getData().values()));
        }
    }

    private void onError(Throwable t) {
        if (championListCallback != null) {
            championListCallback.onChampionsDownloadError(t);
        }
    }

    public void setChampionListCallback(ChampionListCallback championListCallback) {
        this.championListCallback = championListCallback;
    }
}
