package com.frama.lolchampions.champion.list.model;

import java.io.Serializable;

/**
 * Created by Balazs_Maczak on 2/8/2017.
 */
public class ImageDto implements Serializable {
    private int w;
    private String full;
    private String sprite;
    private int h;
    private int x;
    private int y;

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
