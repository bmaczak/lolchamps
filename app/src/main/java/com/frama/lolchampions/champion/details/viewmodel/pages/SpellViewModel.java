package com.frama.lolchampions.champion.details.viewmodel.pages;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import com.frama.lolchampions.champion.details.model.dto.SpellDto;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class SpellViewModel extends ItemViewModel {
    private static final String IMAGE_BASE_URL = "http://ddragon.leagueoflegends.com/cdn/7.4.3/img/spell/";

    private SpellDto spellDto;

    public SpellViewModel(SpellDto spellDto) {
        this.spellDto = spellDto;
    }

    public String getName() {
        return spellDto.getName();
    }

    public Spanned getDescription() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(spellDto.getDescription(), Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(spellDto.getDescription());
        }
    }

    public String getImage() {
        return IMAGE_BASE_URL + spellDto.getImage().getFull();
    }

    protected SpellDto getSpellDto() {
        return spellDto;
    }
}
