package com.frama.lolchampions.champion.details.viewmodel.pages;

import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.model.dto.PassiveSpellViewModel;
import com.frama.lolchampions.champion.details.model.dto.SpellDto;
import com.frama.lolchampions.champion.details.viewmodel.PageViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class SpellsPageViewModel extends PageViewModel {

    public SpellsPageViewModel(ChampionDetailsModel model) {
        super(model);
    }

    public List<SpellViewModel> itemViewModels() {
        List<SpellViewModel> viewModels = new ArrayList<>();
        for (SpellDto spell : getModel().getSpells()) {
            viewModels.add(new SpellViewModel(spell));
        }
        if (!viewModels.isEmpty()) {
            viewModels.add(0, new PassiveSpellViewModel(getModel().getPassive()));
        }
        return viewModels;
    }
}
