package com.frama.lolchampions.champion.list.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.frama.lolchampions.BR;
import com.frama.lolchampions.champion.list.model.ChampionDto;
import com.frama.lolchampions.champion.list.model.ChampionListCallback;
import com.frama.lolchampions.champion.list.model.ChampionListModel;
import com.frama.lolchampions.router.Router;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balazs_Maczak on 2/21/2017.
 */
public class ChampionListViewModel extends BaseObservable implements ChampionListCallback {

    private Router router;
    private ChampionListModel model;
    @Bindable
    private List<ItemViewModel> itemViewModels;

    public ChampionListViewModel(ChampionListModel model, Router router) {
        this.model = model;
        this.router = router;
    }

    public void loadChampions() {
        model.setChampionListCallback(this);
        model.loadChampions();
    }

    public List<ItemViewModel> getItemViewModels() {
        return itemViewModels;
    }

    @Override
    public void onChampionsDownloaded(List<ChampionDto> champions) {
        List<ItemViewModel> items = new ArrayList<>();
        for (ChampionDto champion : champions) {
            items.add(new ChampionListItemViewModel(champion, router));
        }
        setItemViewModels(items);
    }

    @Override
    public void onChampionsDownloadError(Throwable throwable) {
        //TODO Error flow
    }

    public void setItemViewModels(List<ItemViewModel> itemViewModels) {
        this.itemViewModels = itemViewModels;
        notifyPropertyChanged(BR.itemViewModels);
    }
}
