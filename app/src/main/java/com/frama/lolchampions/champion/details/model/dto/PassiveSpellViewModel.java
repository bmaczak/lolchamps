package com.frama.lolchampions.champion.details.model.dto;

import com.frama.lolchampions.champion.details.viewmodel.pages.SpellViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class PassiveSpellViewModel extends SpellViewModel {

    private static final String IMAGE_BASE_URL = "http://ddragon.leagueoflegends.com/cdn/7.4.3/img/passive/";

    public PassiveSpellViewModel(SpellDto spellDto) {
        super(spellDto);
    }

    @Override
    public String getImage() {
        return IMAGE_BASE_URL + getSpellDto().getImage().getFull();
    }
}
