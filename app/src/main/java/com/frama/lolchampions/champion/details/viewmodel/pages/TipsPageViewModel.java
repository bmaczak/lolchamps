package com.frama.lolchampions.champion.details.viewmodel.pages;

import android.text.TextUtils;

import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.viewmodel.PageViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class TipsPageViewModel extends PageViewModel {

    private static String TIPS_DELIMITER = "\n\n";

    public TipsPageViewModel(ChampionDetailsModel model) {
        super(model);
    }

    public String getAllytips() {
        return TextUtils.join(TIPS_DELIMITER, getModel().getAllytips());
    }

    public String getEnemytips() {
        return TextUtils.join(TIPS_DELIMITER, getModel().getEnemytips());
    }
}
