package com.frama.lolchampions.champion.details.viewmodel;

import android.content.res.Resources;
import android.databinding.BaseObservable;

import com.frama.lolchampions.R;
import com.frama.lolchampions.champion.details.model.dto.ChampionDetailsDto;
import com.frama.lolchampions.champion.details.model.ChampionDetailsCallback;
import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.viewmodel.pages.InfoPageViewModel;
import com.frama.lolchampions.champion.details.viewmodel.pages.LorePageViewModel;
import com.frama.lolchampions.champion.details.viewmodel.pages.SpellsPageViewModel;
import com.frama.lolchampions.champion.details.viewmodel.pages.TipsPageViewModel;
import com.frama.lolchampions.champion.list.model.SkinDto;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;
import com.frama.lolchampions.ui.viewpager.ViewPagerPage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class ChampionDetailsViewModel extends BaseObservable implements ChampionDetailsCallback {

    private Resources resources;
    private ChampionDetailsModel model;
    private List<ViewPagerPage> pages;

    public ChampionDetailsViewModel(ChampionDetailsModel championDetailsModel, Resources resources) {
        this.model = championDetailsModel;
        this.resources = resources;
        pages = new ArrayList<>();
        pages.add(new ViewPagerPage(resources.getString(R.string.info), R.layout.champion_details_page_info, new InfoPageViewModel(model)));
        pages.add(new ViewPagerPage(resources.getString(R.string.spells), R.layout.champion_details_page_spells, new SpellsPageViewModel(model)));
        pages.add(new ViewPagerPage(resources.getString(R.string.tips), R.layout.champion_details_page_tips, new TipsPageViewModel(model)));
        pages.add(new ViewPagerPage(resources.getString(R.string.lore), R.layout.champion_details_page_lore, new LorePageViewModel(model)));
    }

    public void loadChampionDetails() {
        model.setChampionDetailsCallback(this);
        model.loadChampionDetails();
    }

    @Override
    public void onChampionDetailsDownloaded(ChampionDetailsDto championDetailsDto) {
        for (ViewPagerPage page : pages) {
            page.getViewModel().notifyChange();
        }
        notifyChange();
    }

    @Override
    public void onChampionDetailsDownloadError(Throwable throwable) {

    }

    public List<ViewPagerPage> pages() {
        return pages;
    }

    public String getName() {
        return model.getName();
    }

    public List<ItemViewModel> getSkinViewModels() {
        List<ItemViewModel> skinViewModels = new ArrayList<>();
        for (SkinDto skin : model.getSkins()) {
            skinViewModels.add(new SkinViewModel(skin, model.getId()));
        }
        return skinViewModels;
    }
}
