package com.frama.lolchampions.champion.details.model.dto;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class ChampionStatsDto {
    private float hp;
    private float hpperlevel;
    private float mp;
    private float mpperlevel;
    private float movespeed;
    private float armor;
    private float armorperlevel;
    private float spellblock;
    private float spellblockperlevel;
    private float attackrange;
    private float hpregen;
    private float hpregenperlevel;
    private float mpregen;
    private float mpregenperlevel;
    private float crit;
    private float critperlevel;
    private float attackdamage;
    private float attackdamageperlevel;
    private float attackspeedoffset;
    private float attackspeedperlevel;

    public float getHp() {
        return hp;
    }

    public float getHpperlevel() {
        return hpperlevel;
    }

    public float getMp() {
        return mp;
    }

    public float getMpperlevel() {
        return mpperlevel;
    }

    public float getMovespeed() {
        return movespeed;
    }

    public float getArmor() {
        return armor;
    }

    public float getArmorperlevel() {
        return armorperlevel;
    }

    public float getSpellblock() {
        return spellblock;
    }

    public float getSpellblockperlevel() {
        return spellblockperlevel;
    }

    public float getAttackrange() {
        return attackrange;
    }

    public float getHpregen() {
        return hpregen;
    }

    public float getHpregenperlevel() {
        return hpregenperlevel;
    }

    public float getMpregen() {
        return mpregen;
    }

    public float getMpregenperlevel() {
        return mpregenperlevel;
    }

    public float getCrit() {
        return crit;
    }

    public float getCritperlevel() {
        return critperlevel;
    }

    public float getAttackdamage() {
        return attackdamage;
    }

    public float getAttackdamageperlevel() {
        return attackdamageperlevel;
    }

    public float getAttackspeedoffset() {
        return attackspeedoffset;
    }

    public float getAttackspeedperlevel() {
        return attackspeedperlevel;
    }
}


