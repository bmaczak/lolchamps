package com.frama.lolchampions.champion.details.viewmodel.pages;

import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.viewmodel.PageViewModel;

import java.util.Locale;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class InfoPageViewModel extends PageViewModel {

    private static String VALUE_FORMAT = "%1$.2f (+%2$.2f)";

    public InfoPageViewModel(ChampionDetailsModel model) {
        super(model);
    }

    public int getAttack() {
        return getModel().getAttack();
    }

    public int getMagic() {
        return getModel().getMagic();
    }

    public int getDefense() {
        return getModel().getDefense();
    }

    public int getDifficulty() {
        return getModel().getDifficulty();
    }

    public String getHp() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getHp(), getModel().getHpperlevel());
    }

    public String getHpregen() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getHpregen(), getModel().getHpregenperlevel());
    }

    public String getMp() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getMp(), getModel().getMpperlevel());
    }

    public String getMpregen() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getMpregen(), getModel().getMpregenperlevel());
    }

    public String getArmor() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getArmor(), getModel().getArmorperlevel());
    }

    public String getSpellblock() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getSpellblock(), getModel().getSpellblockperlevel());
    }

    public String getCrit() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getCrit(), getModel().getCritperlevel());
    }

    public String getAttackdamage() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getAttackdamage(), getModel().getAttackdamageperlevel());
    }

    public String getAttackspeed() {
        return String.format(Locale.US, VALUE_FORMAT, getModel().getAttackspeedoffset(), getModel().getAttackspeedperlevel());
    }

    public String getMovespeed() {
        return String.valueOf(getModel().getMovespeed());
    }

    public String getRange() {
        return String.valueOf(getModel().getAttackrange());
    }
}
