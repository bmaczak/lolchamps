package com.frama.lolchampions.champion.details.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.frama.lolchampions.api.ApiResponse;
import com.frama.lolchampions.api.RiotApi;
import com.frama.lolchampions.champion.details.model.dto.ChampionDetailsDto;
import com.frama.lolchampions.champion.details.model.dto.SpellDto;
import com.frama.lolchampions.champion.list.model.ChampionDto;
import com.frama.lolchampions.champion.list.model.SkinDto;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class ChampionDetailsModel {

    private RiotApi api;
    private ChampionDto championDto;
    private ChampionDetailsDto championDetailsDto;
    private Subscription subscription;
    private ChampionDetailsCallback championDetailsCallback;

    public ChampionDetailsModel(ChampionDto championDto, RiotApi riotApi) {
        this.championDto = championDto;
        this.api = riotApi;
    }

    public void loadChampionDetails() {
        if (subscription == null || subscription.isUnsubscribed()) {
            subscription = api.loadChampionDetails(championDto.getId())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onNext, this::onError);
        }
    }

    private void onNext(ApiResponse<Map<String, ChampionDetailsDto>> response) {
        championDetailsDto = new ArrayList<>(response.getData().values()).get(0);
        if (championDetailsCallback != null) {
            championDetailsCallback.onChampionDetailsDownloaded(championDetailsDto);
        }
    }

    private void onError(Throwable t) {
        if (championDetailsCallback != null) {
            championDetailsCallback.onChampionDetailsDownloadError(t);
        }
    }

    public void setChampionDetailsCallback(ChampionDetailsCallback championDetailsCallback) {
        this.championDetailsCallback = championDetailsCallback;
    }

    public String getName() {
        return championDetailsDto == null ? championDto.getName() : championDetailsDto.getName();
    }

    public String getId() {
        return championDetailsDto == null ? championDto.getId() : championDetailsDto.getId();
    }

    public List<SkinDto> getSkins() {
        return championDetailsDto == null ? new ArrayList<>() : championDetailsDto.getSkins();
    }

    public String getLore() {
        return championDetailsDto == null ? "" : championDetailsDto.getLore();
    }

    public String getBlurb() {
        return championDetailsDto == null ? "" : championDetailsDto.getBlurb();
    }

    public List<String> getAllytips() {
        return championDetailsDto == null ? new ArrayList<>() : championDetailsDto.getAllytips();
    }

    public List<String> getEnemytips() {
        return championDetailsDto == null ? new ArrayList<>() : championDetailsDto.getEnemytips();
    }

    public int getAttack() {
        return championDetailsDto == null ? 0 : championDetailsDto.getInfo().getAttack();
    }

    public int getMagic() {
        return championDetailsDto == null ? 0 : championDetailsDto.getInfo().getMagic();
    }

    public int getDefense() {
        return championDetailsDto == null ? 0 : championDetailsDto.getInfo().getDefense();
    }

    public int getDifficulty() {
        return championDetailsDto == null ? 0 : championDetailsDto.getInfo().getDifficulty();
    }

    public List<SpellDto> getSpells() {
        return championDetailsDto == null ? new ArrayList<>() : championDetailsDto.getSpells();
    }

    public SpellDto getPassive() {
        return championDetailsDto == null ? null : championDetailsDto.getPassive();
    }

    public float getHp() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getHp();
    }

    public float getHpperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getHpperlevel();
    }

    public float getMp() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getMp();
    }

    public float getMpperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getMpperlevel();
    }

    public float getMovespeed() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getMovespeed();
    }

    public float getArmor() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getArmor();
    }

    public float getArmorperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getArmorperlevel();
    }

    public float getSpellblock() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getSpellblock();
    }

    public float getSpellblockperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getSpellblockperlevel();
    }

    public float getAttackrange() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getAttackrange();
    }

    public float getHpregen() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getHpregen();
    }

    public float getHpregenperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getHpregenperlevel();
    }

    public float getMpregen() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getMpregen();
    }

    public float getMpregenperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getMpregenperlevel();
    }

    public float getCrit() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getCrit();
    }

    public float getCritperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getCritperlevel();
    }

    public float getAttackdamage() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getAttackdamage();
    }

    public float getAttackdamageperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getAttackdamageperlevel();
    }

    public float getAttackspeedoffset() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getAttackspeedoffset();
    }

    public float getAttackspeedperlevel() {
        return championDetailsDto == null ? 0 : championDetailsDto.getStats().getAttackspeedperlevel();
    }
}
