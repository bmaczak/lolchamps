package com.frama.lolchampions.api;

/**
 * Created by Balazs_Maczak on 2/8/2017.
 */
public class ApiResponse<T> {
    private T data;
    private String type;
    private String version;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
