package com.frama.lolchampions.api;

import java.util.Map;

import com.frama.lolchampions.champion.details.model.dto.ChampionDetailsDto;
import com.frama.lolchampions.champion.list.model.ChampionDto;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Balazs_Maczak on 12/16/2016.
 */
public interface RiotApi {

    String BASE_URL = "http://ddragon.leagueoflegends.com/cdn/7.4.3/data/en_US/";

    @GET("champion.json")
    Observable<ApiResponse<Map<String, ChampionDto>>> loadChampions();

    @GET("champion/{id}.json")
    Observable<ApiResponse<Map<String, ChampionDetailsDto>>> loadChampionDetails(@Path("id") String championId);
}
