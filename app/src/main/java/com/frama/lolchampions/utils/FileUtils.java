package com.frama.lolchampions.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Balazs_Maczak on 2/8/2017.
 */
public class FileUtils {
    public static String loadAsset(Context context, String inFile) {
        String tContents = "";

        try {
            InputStream stream = context.getAssets().open(inFile);

            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }

        return tContents;

    }
}
