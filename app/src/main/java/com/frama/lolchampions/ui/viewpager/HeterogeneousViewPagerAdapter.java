package com.frama.lolchampions.ui.viewpager;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frama.lolchampions.BR;

import java.util.List;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class HeterogeneousViewPagerAdapter extends PagerAdapter {

    private List<ViewPagerPage> pages;

    public HeterogeneousViewPagerAdapter(List<ViewPagerPage> pages) {
        this.pages = pages;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(collection.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, pages.get(position).getLayoutId(), collection, false);
        binding.setVariable(BR.viewModel, pages.get(position).getViewModel());
        binding.executePendingBindings();
        collection.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return pages.get(position).getTitle();
    }

    public void setPages(List<ViewPagerPage> pages) {
        this.pages = pages;
        notifyDataSetChanged();
    }
}
