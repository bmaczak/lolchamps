package com.frama.lolchampions.ui.recycleview;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.frama.lolchampions.BR;

import java.util.List;

/**
 * Created by Balazs_Maczak on 2/21/2017.
 */
public class RecyclerViewAdapter<T extends ItemViewModel> extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private int itemLayout;
    private List<T> itemViewModels;

    public RecyclerViewAdapter(@LayoutRes int itemLayout, List<T> itemViewModels) {
        this.itemLayout = itemLayout;
        this.itemViewModels = itemViewModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, itemLayout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        final T itemViewModel = itemViewModels.get(position);
        holder.binding.setVariable(BR.viewModel, itemViewModel);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return itemViewModels == null ? 0 : itemViewModels.size();
    }

    public void setItemViewModels(List<T> itemViewModels) {
        this.itemViewModels = itemViewModels;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
