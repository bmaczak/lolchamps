package com.frama.lolchampions.ui.viewpager;

import android.support.annotation.LayoutRes;

import com.frama.lolchampions.ui.recycleview.ItemViewModel;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class ViewPagerPage {
    private CharSequence title;
    private int layoutId;
    private ItemViewModel viewModel;

    public ViewPagerPage(CharSequence title, @LayoutRes int layoutId, ItemViewModel viewModel) {
        this.title = title;
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }

    public CharSequence getTitle() {
        return title;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public ItemViewModel getViewModel() {
        return viewModel;
    }
}
