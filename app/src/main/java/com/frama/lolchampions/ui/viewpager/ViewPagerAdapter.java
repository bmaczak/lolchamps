package com.frama.lolchampions.ui.viewpager;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frama.lolchampions.BR;
import com.frama.lolchampions.ui.recycleview.ItemViewModel;

import java.util.List;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class ViewPagerAdapter<T extends ItemViewModel> extends PagerAdapter {

    private int itemLayout;
    private List<T> itemViewModels;

    public ViewPagerAdapter(@LayoutRes int itemLayout, List<T> itemViewModels) {
        this.itemLayout = itemLayout;
        this.itemViewModels = itemViewModels;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(collection.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, itemLayout, collection, false);
        binding.setVariable(BR.viewModel, itemViewModels.get(position));
        binding.executePendingBindings();
        collection.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return itemViewModels.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return itemViewModels.get(position).toString();
    }

    public void setItemViewModels(List<T> itemViewModels) {
        this.itemViewModels = itemViewModels;
        notifyDataSetChanged();
    }
}
