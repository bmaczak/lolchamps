package com.frama.lolchampions.application;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.frama.lolchampions.di.components.ApplicationComponent;

/**
 * Created by Balazs_Maczak on 12/16/2016.
 */
public class LolChampionsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponent.Injector.inject(this);
        Fresco.initialize(this);
    }
}
