package com.frama.lolchampions.router;

import com.frama.lolchampions.champion.details.ChampionDetailsActivity;
import com.frama.lolchampions.champion.list.model.ChampionDto;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by Balazs_Maczak on 2/22/2017.
 */
public class Router {
    private Activity activity;

    public Router(Activity activity) {
        this.activity = activity;
    }

    public void openChampionDetails(ChampionDto championDto) {
        activity.startActivity(ChampionDetailsActivity.createNavigationIntent(activity, championDto));
    }
}
