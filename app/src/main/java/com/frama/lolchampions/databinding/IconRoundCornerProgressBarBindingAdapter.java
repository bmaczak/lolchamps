package com.frama.lolchampions.databinding;

import android.databinding.BindingAdapter;
import android.os.Handler;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;

/**
 * Created by Balazs on 2017. 02. 26..
 */

public class IconRoundCornerProgressBarBindingAdapter {

    //Delay is needed when setting the progress due to the implementation of IconRoundCornerProgressBar
    @BindingAdapter({"progress"})
    public static void setProgress(IconRoundCornerProgressBar progressBar, int progress) {
        final Handler handler = new Handler();
        handler.postDelayed(() -> progressBar.setProgress(progress), 100);
    }

}
