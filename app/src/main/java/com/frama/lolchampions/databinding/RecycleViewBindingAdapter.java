package com.frama.lolchampions.databinding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.frama.lolchampions.ui.recycleview.ItemViewModel;
import com.frama.lolchampions.ui.recycleview.RecyclerViewAdapter;

import java.util.List;

/**
 * Created by Balazs_Maczak on 2/21/2017.
 */
public class RecycleViewBindingAdapter {

    @BindingAdapter({"itemLayout", "itemViewModels"})
    public static <T extends ItemViewModel> void setItemViewModels(RecyclerView recyclerView, int itemLayout, List<T> itemViewModels) {
        RecyclerViewAdapter<T> adapter = (RecyclerViewAdapter<T>) recyclerView.getAdapter();
        if (adapter == null) {
            adapter = new RecyclerViewAdapter<>(itemLayout, itemViewModels);
            recyclerView.setAdapter(adapter);
        }
        adapter.setItemViewModels(itemViewModels);

    }
}
