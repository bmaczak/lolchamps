package com.frama.lolchampions.databinding;

import java.util.List;

import com.frama.lolchampions.ui.recycleview.ItemViewModel;
import com.frama.lolchampions.ui.viewpager.HeterogeneousViewPagerAdapter;
import com.frama.lolchampions.ui.viewpager.ViewPagerAdapter;
import com.frama.lolchampions.ui.viewpager.ViewPagerPage;

import android.databinding.BindingAdapter;
import android.support.v4.view.ViewPager;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
public class ViewPagerBindingAdapter {

    @BindingAdapter({"itemLayout", "itemViewModels"})
    public static <T extends ItemViewModel> void setItemViewModels(ViewPager viewPager, int itemLayout, List<T> itemViewModels) {
        ViewPagerAdapter<T> adapter = (ViewPagerAdapter<T>) viewPager.getAdapter();
        if (adapter == null) {
            adapter = new ViewPagerAdapter<>(itemLayout, itemViewModels);
            viewPager.setAdapter(adapter);
        }
        adapter.setItemViewModels(itemViewModels);
    }

    @BindingAdapter({"pages"})
    public static void setPages(ViewPager viewPager, List<ViewPagerPage> pages) {
        HeterogeneousViewPagerAdapter adapter = (HeterogeneousViewPagerAdapter) viewPager.getAdapter();
        if (adapter == null) {
            adapter = new HeterogeneousViewPagerAdapter(pages);
            viewPager.setAdapter(adapter);
        }
        adapter.setPages(pages);
    }
}
