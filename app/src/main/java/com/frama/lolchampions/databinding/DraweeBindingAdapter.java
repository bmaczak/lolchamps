package com.frama.lolchampions.databinding;

import com.facebook.drawee.view.SimpleDraweeView;

import android.databinding.BindingAdapter;
import android.net.Uri;

/**
 * Created by Balazs_Maczak on 2/22/2017.
 */
public class DraweeBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(SimpleDraweeView draweeView, String imageUrl) {
        draweeView.setImageURI(Uri.parse(imageUrl));
    }

}
