package com.frama.lolchampions.di.modules;

import javax.inject.Singleton;

import com.frama.lolchampions.api.RiotApi;
import com.frama.lolchampions.di.qualifiers.ApplicationContext;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Balazs_Maczak on 12/16/2016.
 */
@Module
public class ApiModule {
    @Provides
    @Singleton
    RiotApi provideRiotApi(Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(RiotApi.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(RiotApi.class);
    }

    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }
}
