package com.frama.lolchampions.di.modules;

import android.content.res.Resources;

import com.frama.lolchampions.api.RiotApi;
import com.frama.lolchampions.champion.details.ChampionDetailsActivity;
import com.frama.lolchampions.champion.details.model.ChampionDetailsModel;
import com.frama.lolchampions.champion.details.viewmodel.ChampionDetailsViewModel;
import com.frama.lolchampions.champion.list.model.ChampionDto;
import com.frama.lolchampions.di.scopes.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
@Module
public class DetailsActivityModule {
    private ChampionDetailsActivity detailsActivity;

    public DetailsActivityModule(ChampionDetailsActivity detailsActivity) {
        this.detailsActivity = detailsActivity;
    }

    @PerActivity
    @Provides
    ChampionDetailsViewModel provideDetailsViewModel(Resources resources, ChampionDetailsModel championDetailsModel) {
        return new ChampionDetailsViewModel(championDetailsModel, resources);
    }

    @PerActivity
    @Provides
    ChampionDetailsModel provideDetailsModel(ChampionDto championDto, RiotApi riotApi) {
        return new ChampionDetailsModel(championDto, riotApi);
    }

    @PerActivity
    @Provides
    ChampionDto provideChampionDto() {
        return ChampionDetailsActivity.extractChampionDto(detailsActivity.getIntent());
    }
}
