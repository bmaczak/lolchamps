package com.frama.lolchampions.di.components;

import com.frama.lolchampions.champion.details.ChampionDetailsActivity;
import com.frama.lolchampions.di.modules.DetailsActivityModule;
import com.frama.lolchampions.di.scopes.PerActivity;

import dagger.Component;

/**
 * Created by Balazs_Maczak on 2/23/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {DetailsActivityModule.class})
public interface DetailsActivityComponent {
    void inject(ChampionDetailsActivity detailsActivity);

    final class Injector {
        private static DetailsActivityComponent activityComponent;

        private Injector() {
        }

        public static void inject(ChampionDetailsActivity activity) {
            activityComponent = DaggerDetailsActivityComponent.builder()
                    .applicationComponent(ApplicationComponent.Injector.getComponent())
                    .detailsActivityModule(new DetailsActivityModule(activity))
                    .build();
            activityComponent.inject(activity);
        }
    }

}
