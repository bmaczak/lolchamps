package com.frama.lolchampions.di.components;

import android.content.Context;
import android.content.res.Resources;

import com.frama.lolchampions.api.RiotApi;
import com.frama.lolchampions.application.LolChampionsApplication;
import com.frama.lolchampions.di.modules.ApiModule;
import com.frama.lolchampions.di.modules.ApplicationModule;
import com.frama.lolchampions.di.qualifiers.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Balazs_Maczak on 12/15/2016.
 */
@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {

    void inject(LolChampionsApplication lolChampionsApplication);

    @ApplicationContext
    Context context();

    Resources resources();

    RiotApi riotApi();

    final class Injector {
        private static ApplicationComponent applicationComponent;

        private Injector() {

        }

        public static void inject(LolChampionsApplication lolChampionsApplication) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(lolChampionsApplication))
                    .build();
            applicationComponent.inject(lolChampionsApplication);
        }

        public static ApplicationComponent getComponent() {
            return applicationComponent;
        }
    }

}
