package com.frama.lolchampions.di.modules;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.frama.lolchampions.api.RiotApi;
import com.frama.lolchampions.champion.list.ChampionListActivity;
import com.frama.lolchampions.champion.list.model.ChampionListModel;
import com.frama.lolchampions.champion.list.viewmodel.ChampionListViewModel;
import com.frama.lolchampions.di.qualifiers.ActivityContext;
import com.frama.lolchampions.di.scopes.PerActivity;
import com.frama.lolchampions.router.Router;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Balazs_Maczak on 1/4/2017.
 */
@Module
public class ListActivityModule {

    private ChampionListActivity listActivity;

    public ListActivityModule(ChampionListActivity listActivity) {
        this.listActivity = listActivity;
    }

    @PerActivity
    @Provides
    ChampionListViewModel provideListViewModel(ChampionListModel model, Router router) {
        return new ChampionListViewModel(model, router);
    }

    @PerActivity
    @Provides
    ChampionListModel provideListModel(RiotApi api) {
        return new ChampionListModel(api);
    }

    @PerActivity
    @Provides
    Router provideRouter() {
        return new Router(listActivity);
    }

    @PerActivity
    @Provides
    @ActivityContext
    Context provideContext() {
        return listActivity;
    }

}
