package com.frama.lolchampions.di.components;

import com.frama.lolchampions.champion.list.ChampionListActivity;
import com.frama.lolchampions.di.modules.ListActivityModule;
import com.frama.lolchampions.di.scopes.PerActivity;

import dagger.Component;

/**
 * Created by Balazs_Maczak on 1/4/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ListActivityModule.class})
public interface ListActivityComponent {

    void inject(ChampionListActivity listActivity);

    final class Injector {
        private static ListActivityComponent activityComponent;

        private Injector() {
        }

        public static void inject(ChampionListActivity activity) {
            activityComponent = DaggerListActivityComponent.builder()
                    .applicationComponent(ApplicationComponent.Injector.getComponent())
                    .listActivityModule(new ListActivityModule(activity))
                    .build();
            activityComponent.inject(activity);
        }
    }
}
